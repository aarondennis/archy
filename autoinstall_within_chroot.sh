#!/bin/bash

# This script is used to do configurations within chroot environment

# Exit on first error
set -e

# Locale
echo "en_AU.UTF-8 UTF-8" > /etc/locale.gen
echo LANG=en_AU.UTF-8 > /etc/locale.conf

# Export substituting your chosen locale
export LANG=en_AU.UTF-8

# Console font
# To make them available after reboot:
echo "FONT=Lat2-Terminus16" > /etc/vconsole.conf

# Time zone
ln -s /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime

# Hardware clock
hwclock --systohc --utc

# Hostname, set the hostname to your liking
echo myhostname > /etc/hostname

# Add the same hostname to /etc/hosts:
echo "#
# /etc/hosts: static lookup table for host names
#

#<ip-address>	<hostname.domain.org>	<hostname>
127.0.0.1	localhost.localdomain	localhost	myhostname
::1		localhost.localdomain	localhost

# End of file" > /etc/hosts

# Configure the network (for the installed system)
# Wired + Dynamic IP
systemctl enable dhcpcd.service

# Install and configure a bootloader
# GRUB
pacman -S --noconfirm grub
grub-install --target=i386-pc --recheck /dev/sda
# To automatically search for other operating systems on your computer, 
# install os-prober (pacman -S os-prober) before running the next command.
pacman -S --noconfirm os-prober
grub-mkconfig -o /boot/grub/grub.cfg

# Remove reduntant GRUB menu entries
# chmod -x /etc/grub.d/10_linux
# grub-mkconfig -o /boot/grub/grub.cfg

# Install packages ============================================================
# Install alsa-utils (which contains alsamixer) to unmuted ALSA
pacman -S --noconfirm alsa-utils

# Install X
# To install the base Xorg packages: 
pacman -S --noconfirm xorg-server xorg-server-utils xorg-xinit
# Basic environment
pacman -S --noconfirm xorg-twm xorg-xclock xterm
# Install mesa for 3D support:
pacman -S --noconfirm mesa

# Install a video driver
# Install the default video driver
pacman -S --noconfirm xf86-video-vesa

# install a set of TrueType fonts
# DejaVu is a set of high quality, general-purpose fonts 
# with good Unicode coverage
pacman -S --noconfirm ttf-dejavu

# Install vim
pacman -S --noconfirm vim

# Install Network Time Protocol (ntp) to sync time
pacman -S --noconfirm ntp

# Install a Window Manager

# User management =============================================================
# adding a new user named archie and specifying Bash as the login shell
# This command will automtically create a group called archie with the same 
# GID as the UID of the user archie and makes this the default group for 
# archie on login. 
# useradd -m -g [initial_group] -G [additional_groups] -s [login_shell] [username]
    # -g defines the group name or number of the user's initial login group. If specified, the group name must exist; if a group number is provided, it must refer to an already existing group. If not specified, the behaviour of useradd will depend on the USERGROUPS_ENAB variable contained in /etc/login.defs. The default behaviour (USERGROUPS_ENAB yes) is to create a group with the same name as the username, with GID equal to UID. 
    # -m creates the user home directory as /home/username. Within their home directory, a non-root user can write files, delete them, install programs, and so on. 
    # -G introduces a list of supplementary groups which the user is also a member of. Each group is separated from the next by a comma, with no intervening spaces. The default is for the user to belong only to the initial group. 
    # -s defines the path and file name of the user's default login shell. After the boot process is complete, the default login shell is the one specified here. Ensure the chosen shell package is installed if choosing something other than Bash. 
useradd -m -G wheel -s /bin/bash archie
# useradd -m -s /bin/bash archie

# Install and setup sudo ======================================================
pacman -S --noconfirm sudo
# visudo must be used to edit /etc/sudoers
# Change default editor for visudo to vim. NOTE: this is NOT needed if sudoers
# files is set correctly.
# EDITOR=vim
# Replace `sudoers` file with the pre-configed one
cp /sudoers /etc/sudoers

# Allow users to shutdown, reboot =============================================
pacman -S --noconfirm polkit

# Change password
echo 'Set the password for archie:'
passwd archie

# Set the root password (e.g. spam12345)
echo 'Set the password for root:'
passwd
