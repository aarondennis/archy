#!/bin/bash

# adding a new user named archie and specifying Bash as the login shell
# This command will automtically create a group called archie with the same 
# GID as the UID of the user archie and makes this the default group for 
# archie on login. 
# useradd -m -g [initial_group] -G [additional_groups] -s [login_shell] [username]
    # -g defines the group name or number of the user's initial login group. If specified, the group name must exist; if a group number is provided, it must refer to an already existing group. If not specified, the behaviour of useradd will depend on the USERGROUPS_ENAB variable contained in /etc/login.defs. The default behaviour (USERGROUPS_ENAB yes) is to create a group with the same name as the username, with GID equal to UID. 
    # -m creates the user home directory as /home/username. Within their home directory, a non-root user can write files, delete them, install programs, and so on. 
    # -G introduces a list of supplementary groups which the user is also a member of. Each group is separated from the next by a comma, with no intervening spaces. The default is for the user to belong only to the initial group. 
    # -s defines the path and file name of the user's default login shell. After the boot process is complete, the default login shell is the one specified here. Ensure the chosen shell package is installed if choosing something other than Bash. 
useradd -m -G wheel -s /bin/bash archie
# useradd -m -s /bin/bash archie


# Install and setup sudo
pacman -S --noconfirm sudo
# visudo must be used to edit /etc/sudoers
# Change default editor for visudo to vim. NOTE: this is NOT needed if sudoers
# files is set correctly.
# EDITOR=vim
# Replace `sudoers` file with the pre-configed one
cp sudoers /etc/sudoers

# Change password
passwd archie

