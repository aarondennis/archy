#!/bin/bash
# This script was used on Virtualhost with Archlinux guest. For other usage on
# a different machines, please review the script.

# Some important assumtions:
#   BIOS motherboard
#   Wired network
#   8GB hard disk (Auto detect)
#   GPT-partitioned drive
#   GRUB bootloader

# Exit on first error
set -e

echo "My script started!"



# Establish an internet connection
# The dhcpcd network daemon starts automatically during boot and it will 
# attempt to start a wired connection. For other network setups (static IP,
# wireless), please see https://wiki.archlinux.org/index.php/Beginners'_guide

# umount if necessary
if list=`mount | grep sda`; then

    echo "$list"

    echo "$list" | while read i; do 
        device_path=`echo $i | cut -d' ' -f1`
        echo "Unmount: $device_path"
        umount $device_path
    done
fi


# Prepare the storage drive
# GPT is choosen over MBR for this script. 
# Assume this is a BIOS motherboard.
# Assume the hard disk is identified as sda.
# sgdisk is used to create partions. For more information, please visit:
# http://www.rodsbooks.com/gdisk/sgdisk-walkthrough.html
# Some options:
# -o                    Erase all GPT data structures and create a fresh GPT.
# -g                    Convert an MBR or BSD disklabel disk to GPT format.
# -p                    Display the current partition table.
# -n partnum:start:end  Create a new partition, numbered partnum, starting at 
#                       sector start and ending at sector end.
sgdisk -og -p /dev/sda

# The command below was used create 2 partions on a 8GB hard disk.
# First partion is needed to setup GRUB on a GPT-partitioned drive in the case
# of using a BIOS motherboard. Second partion is for system files and user
# files.
# NOTE: Have to create second partions before the first.

# -E option: Display the sector number at the end of the largest empty block of sectors on the disk.
last_sector=`sgdisk -E /dev/sda | sed -n 2p`

# sgdisk -n 2:2048:16777182 -p /dev/sda
sgdisk -n 2:2048:$last_sector -p /dev/sda

sgdisk -n 1:34:2047 -t 1:ef02 -p /dev/sda

# Create filesystems for the second partition.
mkfs.ext4 -F /dev/sda2

# Mount the partition(s)
mount /dev/sda2 /mnt

# synchronize your clock with the network, ignoring large deviations between
# local UTC and network UTC. 
# ntpd -qg


# Select a mirror
# Before installing, you may want to edit the mirrorlist file and place your 
# preferred mirror first.
echo '##
## Arch Linux repository mirrorlist
## Sorted by mirror score from mirror status page
## Generated on 2014-03-31
##

## Score: 3.5, Viet Nam
Server = http://mirror-fpt-telecom.fpt.net/archlinux/$repo/os/$arch
## Score: 1.6, Australia
Server = http://mirror.internode.on.net/pub/archlinux/$repo/os/$arch
## Score: 1.8, Australia
Server = http://mirror.aarnet.edu.au/pub/archlinux/$repo/os/$arch
## Score: 2.1, Australia
Server = http://ftp.swin.edu.au/archlinux/$repo/os/$arch
## Score: 4.3, Australia
Server = http://archlinux.mirror.uber.com.au/$repo/os/$arch
## Score: 5.0, Australia
Server = http://ftp.iinet.net.au/pub/archlinux/$repo/os/$arch
## Score: 5.5, Australia
Server = http://mirror.optus.net/archlinux/$repo/os/$arch
## Score: 12.8, Australia
Server = http://syd.mirror.rackspace.com/archlinux/$repo/os/$arch
## Score: 5.1, Singapore
Server = http://mirror.nus.edu.sg/archlinux/$repo/os/$arch
## Score: 2.8, Singapore
Server = http://download.nus.edu.sg/mirror/arch/$repo/os/$arch' > /etc/pacman.d/mirrorlist

# Refresh all package lists, maybe unnecessary for new installations
# pacman -Syy

# Install the base system
# base-devel includes packages to build from the AUR.
pacstrap /mnt base base-devel

# Generate an fstab
genfstab -U -p /mnt >> /mnt/etc/fstab

# Copy autoinstal_within_chroot.sh and sudoers to the chroot environment
cp archy.git/autoinstall_within_chroot.sh /mnt/
cp archy.git/sudoers /mnt/

# Run the configuration script within chroot environment 
arch-chroot /mnt /autoinstall_within_chroot.sh

# Unmount the partitions and reboot
umount -R /mnt
