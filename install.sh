#!/bin/bash

# set -e

# synchronize your clock with the network, ignoring large deviations between
# local UTC and network UTC. 
ntpd -qg

# Enable dhcp on Android USB tethering
dhcpcd enp0s16f3u2

cd ~/

curl -L -k https://gitlab.com/goohobot/archy/repository/archive.tar.gz?ref=master > archive.tar.gz
tar -xvzf archive.tar.gz
archy.git/autoinstall.sh
