#!/bin/bash

set -e

sudo mkdir -p /mnt/iso

if [ ! -e archiso/archlinux-*.iso ]; then
    echo "The iso file is NOT found!"
    exit 1
fi

sudo mount -o loop archiso/archlinux-*.iso /mnt/iso

# Choose the device
lsblk
echo 'Enter the USB device name (without "/dev/":'
read device

device="/dev/$device"

# Unmount the partitions
list=`mount | grep $device`
for i in $list; do
    device_path=`echo $i | cut -d' ' -f1`
    echo "Unmount: $device_path"
    sudo umount $device_path
done

# Delete partions
sudo parted -s $device mklabel msdos

# Create a partition
sudo parted -s $device mkpart primary fat32 0 1969
# Mark it bootable
sudo parted $device set 1 boot on

# formart $device
sudo mkdosfs -n 'USB_LABEL' -I -F 32 ${device}1



# Create partions
# sudo parted -s $device mklabel msdos
# sudo parted -s $device mkpart primary fat32 0 512
# sudo parted -s $device mkpart primary fat32 512 1028
# sudo parted -s $device mkpart primary fat32 1028 1969

# Copy files
sudo mkdir -p /mnt/usb
sudo mount ${device}1 /mnt/usb
sudo cp -a /mnt/iso/* /mnt/usb
sync

# To boot either a label or an UUID to select the partition to boot from is
# required. By default the label ARCH_2014XX (with the appropriate release
# month) is used. Thus, the partition’s label has to be set accordingly, for
# example using gparted. Alternatively, you can change this behaviour by
# altering the lines ending by archisolabel=ARCH_2014XX in files
# /mnt/usb/arch/boot/syslinux/archiso_sys32.cfg and archiso_sys64.cfg, as well
# as /mnt/usb/loader/entries/archiso-x86_64.conf or similar for a 32-bit ISO
# (the last being useful only, if you want to boot the USB flash device from an
# EFI system). To use an UUID instead, replace those portions of lines with
# archisodevice=/dev/disk/by-uuid/YOUR-UUID. The UUID can be retrieved with
# blkid -o value -s UUID /dev/sdXn. 

sudo cp archiso/archiso_sys64.cfg /mnt/usb/arch/boot/syslinux/archiso_sys64.cfg
sudo cp archiso/archiso_sys32.cfg /mnt/usb/arch/boot/syslinux/archiso_sys32.cfg
sudo cp archiso/archiso-x86_64.conf /mnt/usb/loader/entries/archiso-x86_64.conf

# Install syslinux
sudo cp -r /usr/lib/syslinux/*.c32 /mnt/usb/arch/boot/syslinux/

sudo apt-get install -y extlinux
sudo extlinux --install /mnt/usb/arch/boot/syslinux

# Fixing: Could not find kernel image: boot/syslinux/whichsys.c32
sed -i "s|../../|/arch|" /mnt/usb/arch/boot/syslinux/syslinux.cfg

sudo umount /mnt/usb

# Mark the partition is bootable


# Install MBR partition table
sudo dd bs=440 count=1 conv=notrunc if=/usr/lib/syslinux/mbr.bin of=$device

# Unmount the ISO file
sudo umount /mnt/iso
